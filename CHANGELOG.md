# Changelog



## [v1.2.2] - 2022-06-15

- Updated the scope of xml-apis [#23510]



## [v1.2.1] - 2020-06-18

- Updated to support new detachedres-library [#19440]
- Disabled Zoom for charts [#19161]



## [v1.2.0] - 2020-04-08

- Updated export csv support with Firefox and Safari [#18034]
- Added Core Services support [#18291]
- Added Detached REs support [#18815]
- Added the alphabetical sorting of tabs [#18754]
- Added TOC menu in reports [#18753]



## [v1.1.0] - 2019-10-01

- Updated to support accounting data visualization at the infrastructure level [#17847]



## [v1.0.0] - 2018-08-01

- First Release



This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
