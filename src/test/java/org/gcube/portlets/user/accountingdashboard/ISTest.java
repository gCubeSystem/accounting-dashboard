/**
 * 
 */
package org.gcube.portlets.user.accountingdashboard;

import java.io.File;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.gcube.portlets.user.accountingdashboard.server.is.AccountingDashboardConfigJAXB;
import org.gcube.portlets.user.accountingdashboard.server.is.BuildInfraNode;
import org.gcube.portlets.user.accountingdashboard.server.is.InfraNodeJAXB;
import org.gcube.portlets.user.accountingdashboard.shared.Constants;
import org.gcube.portlets.user.accountingdashboard.shared.is.InfraNode;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import junit.framework.TestCase;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class ISTest extends TestCase {

	private static Logger logger = LoggerFactory.getLogger(ISTest.class);

	@Test
	public void testAccountingDashboardResource() {
		if (Constants.TEST_ENABLE) {
			logger.debug("Test Enabled");

			try {
				logger.debug("Scope: " + Constants.DEFAULT_SCOPE);
				InfraNode infraNode = BuildInfraNode.build(Constants.DEFAULT_SCOPE);
				logger.debug("Infra Node: " + infraNode);
				assertTrue(true);

			} catch (Exception e) {
				logger.error(e.getLocalizedMessage(), e);
				assertTrue("Error searching the resource!", false);
			}

		} else {
			logger.debug("Test Disabled");
			assertTrue(true);
		}
	}

	@Test
	public void testInfraNodeMarshaller() {
		if (Constants.TEST_ENABLE) {
			logger.debug("Test Enabled");

			try {
				logger.info("Check Marshalling");

				InfraNodeJAXB infraNodeCoreServices = new InfraNodeJAXB();
				infraNodeCoreServices.setScope("CoreServices");
				infraNodeCoreServices.setName("Core Services");

				InfraNodeJAXB infraNodeCatalogue = new InfraNodeJAXB();
				infraNodeCatalogue.setScope("Catalogue");
				infraNodeCatalogue.setName("Catalogue");

				InfraNodeJAXB infraNodeWorkspace = new InfraNodeJAXB();
				infraNodeWorkspace.setScope("Workspace");
				infraNodeWorkspace.setName("Workspace");

				InfraNodeJAXB infraNodeMessages = new InfraNodeJAXB();
				infraNodeMessages.setScope("Messages");
				infraNodeMessages.setName("Messages");

				ArrayList<InfraNodeJAXB> children = new ArrayList<>();
				children.add(infraNodeCatalogue);
				children.add(infraNodeWorkspace);
				children.add(infraNodeMessages);
				infraNodeCoreServices.setChildren(children);

				AccountingDashboardConfigJAXB config = new AccountingDashboardConfigJAXB();
				config.setEnabledInfraNode(true);
				config.setBaseInfraNode(infraNodeCoreServices);

				JAXBContext jaxbContext = JAXBContext.newInstance(AccountingDashboardConfigJAXB.class);
				Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
				jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

				// Print XML String to Console
				StringWriter sw = new StringWriter();

				// Write XML to StringWriter
				jaxbMarshaller.marshal(config, sw);

				// Verify XML Content
				String xmlContent = sw.toString();
				logger.debug(xmlContent);

				logger.info("Check Unmarshalling");
				Path path = Files.createTempFile("AccountingDashboardConfig", ".xml");
				File file = path.toFile();
				jaxbMarshaller.marshal(config, file);

				Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
				AccountingDashboardConfigJAXB configUnmarshalled = (AccountingDashboardConfigJAXB) jaxbUnmarshaller
						.unmarshal(file);
				logger.debug("Config unmarshallded: " + configUnmarshalled);
				file.delete();
				logger.info("Success!");
				assertTrue(true);

			} catch (Throwable e) {
				logger.error(e.getLocalizedMessage(), e);
				assertTrue("Error in InfraNode Marshal!", false);
			}
		} else {
			logger.debug("Test Disabled");
			assertTrue(true);
		}
	}

}
