/**
 * 
 */
package org.gcube.portlets.user.accountingdashboard;

import org.gcube.infrastructure.detachedres.detachedreslibrary.server.DetachedREsClient;
import org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re.DetachedREs;
import org.gcube.portlets.user.accountingdashboard.shared.Constants;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import junit.framework.TestCase;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class ISDetachedREsTest extends TestCase {

	private static Logger logger = LoggerFactory.getLogger(ISDetachedREsTest.class);

	@Test
	public void testAccountingDashboardResource() {
		if (Constants.TEST_ENABLE) {
			logger.debug("Test Enabled");

			try {
				DetachedREsClient detachedREsClient = new DetachedREsClient(Constants.DEFAULT_TOKEN);
				DetachedREs detachedREs = detachedREsClient.getDetachedREsInScope(Constants.DEFAULT_SCOPE);
				logger.debug("DetachedREs: " + detachedREs);
				assertTrue(true);

			} catch (Exception e) {
				logger.error(e.getLocalizedMessage(), e);
				assertTrue("Error searching the resource!", false);
			}

		} else {
			logger.debug("Test Disabled");
			assertTrue(true);
		}
	}



}
