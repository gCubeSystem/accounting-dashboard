package org.gcube.portlets.user.accountingdashboard.client.application;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.gcube.portlets.user.accountingdashboard.client.application.controller.Controller;
import org.gcube.portlets.user.accountingdashboard.client.application.menu.MenuPresenter;
import org.gcube.portlets.user.accountingdashboard.client.place.NameTokens;

import com.google.gwt.user.client.Window;
import com.google.inject.Inject;
import com.google.web.bindery.event.shared.EventBus;
import com.gwtplatform.mvp.client.Presenter;
import com.gwtplatform.mvp.client.View;
import com.gwtplatform.mvp.client.annotations.ProxyStandard;
import com.gwtplatform.mvp.client.presenter.slots.NestedSlot;
import com.gwtplatform.mvp.client.presenter.slots.PermanentSlot;
import com.gwtplatform.mvp.client.proxy.NavigationEvent;
import com.gwtplatform.mvp.client.proxy.NavigationHandler;
import com.gwtplatform.mvp.client.proxy.Proxy;
import com.gwtplatform.mvp.shared.proxy.PlaceRequest;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class ApplicationPresenter
		extends Presenter<ApplicationPresenter.PresenterView, ApplicationPresenter.ApplicationPresenterProxy>
		implements NavigationHandler {

	private static Logger logger = java.util.logging.Logger.getLogger("");

	interface PresenterView extends View {
	}

	@ProxyStandard
	interface ApplicationPresenterProxy extends Proxy<ApplicationPresenter> {
	}

	public static final PermanentSlot<MenuPresenter> SLOT_MENU = new PermanentSlot<>();
	public static final NestedSlot SLOT_MAIN = new NestedSlot();

	private MenuPresenter menuPresenter;
	private Controller controller;

	@Inject
	ApplicationPresenter(EventBus eventBus, PresenterView view, ApplicationPresenterProxy proxy,
			MenuPresenter menuPresenter, Controller controller) {
		super(eventBus, view, proxy, RevealType.Root);
		this.menuPresenter = menuPresenter;
		this.controller = controller;
		callHello();
	}

	@Override
	protected void onBind() {
		super.onBind();

		setInSlot(SLOT_MENU, menuPresenter);
		addRegisteredHandler(NavigationEvent.getType(), this);

	}

	@Override
	protected void onReveal() {
		super.onReveal();
	}

	@Override
	public void onNavigation(NavigationEvent navigationEvent) {
		if (navigationEvent != null) {
			PlaceRequest placeRequest = navigationEvent.getRequest();
			logger.log(Level.FINE, "Navigation: " + placeRequest);
			// If the navigation is related to mainarea scrollbar is not reset
			if (placeRequest.getNameToken().compareTo(NameTokens.getMainArea()) != 0) {
				Window.scrollTo(0, 0);
			}
			// placeManager.revealPlace(placeRequest);
		} else {
			Window.scrollTo(0, 0);
		}
	}

	private void callHello() {
		controller.callHello();
	}

}
