package org.gcube.portlets.user.accountingdashboard.shared;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class Constants {

	public static final boolean DEBUG_MODE = false;
	public static final boolean TEST_ENABLE = false;

	public static final String APPLICATION_ID = "org.gcube.portlets.user.accountingdashboard.server.portlet.AccountingDashboardPortlet";
	public static final String CMEMS_IMPORTER_ID = "AccountingDashboardId";
	public static final String CMEMS_IMPORTER_COOKIE = "AccountingDashboardLangCookie";
	public static final String CMEMS_IMPORTER_LANG = "AccountingDashBoradLang";

	public static final String DEFAULT_USER = "giancarlo.panichi";
	//public static final String DEFAULT_SCOPE = "/gcube/devNext/NextNext";
	public static final String DEFAULT_SCOPE = "/gcube";
	public static final String DEFAULT_TOKEN = "";
	public static final String DEFAULT_ROLE = "OrganizationMember";

	//
	public static final int PAGE_SIZE_DEFAULT = 20;
	public static final int PAGE_SIZE_IN_FORM_DEFAULT = 10;

	// Session
	public static final String CURR_GROUP_ID = "CURR_GROUP_ID";
	public static final String CURR_USER_ID = "CURR_USER_ID";

	// Service
	public static final int CLIENT_MONITOR_PERIODMILLIS = 2000;

	// IS Resource
	public static final String ACCOUNTING_DASHBOARD_NAME = "AccountingDashboard";
	public static final String ACCOUNTING__DASHBOARD_CATEGORY = "AccountingProfile";

}
