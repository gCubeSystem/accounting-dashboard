package org.gcube.portlets.user.accountingdashboard.shared.options;

import java.io.Serializable;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class TreeOptions implements Serializable {

	private static final long serialVersionUID = 730133583251742872L;

	private boolean showDetachedREs = false;
	
	public TreeOptions() {
		super();
	}

	public TreeOptions(boolean showDetachedREs) {
		super();
		this.showDetachedREs = showDetachedREs;
	}

	public boolean isShowDetachedREs() {
		return showDetachedREs;
	}

	public void setShowDetachedREs(boolean showDetachedREs) {
		this.showDetachedREs = showDetachedREs;
	}

	@Override
	public String toString() {
		return "TreeOptions [showDetachedREs=" + showDetachedREs + "]";
	}

}
