package org.gcube.portlets.user.accountingdashboard.client.application.dialog.info;


import com.gwtplatform.mvp.client.UiHandlers;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public interface InfoUiHandlers extends UiHandlers{
	
}
