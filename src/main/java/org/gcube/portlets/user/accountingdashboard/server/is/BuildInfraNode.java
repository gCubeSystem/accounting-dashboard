package org.gcube.portlets.user.accountingdashboard.server.is;

import java.util.ArrayList;

import org.gcube.portlets.user.accountingdashboard.shared.Constants;
import org.gcube.portlets.user.accountingdashboard.shared.exception.ServiceException;
import org.gcube.portlets.user.accountingdashboard.shared.is.InfraNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class BuildInfraNode {

	private static Logger logger = LoggerFactory.getLogger(BuildInfraNode.class);

	public static InfraNode build(String scope) throws ServiceException {
		logger.info("Build Infra Nodes");
		InfraNode infraNode = null;

		if (Constants.DEBUG_MODE) {
			logger.info("AccountDashboard: use debug configuration for infra nodes.");
			infraNode = useDefaultConfiguration();
		} else {
			AccountingDashboardConfigJAXB accountingDashboardConfigJAXB = null;
			try {
				accountingDashboardConfigJAXB = InformationSystemUtils.retrieveAccountingDashboardConfig(scope);
			} catch (ServiceException e) {
				logger.debug(e.getLocalizedMessage(), e);
			}

			if (accountingDashboardConfigJAXB != null) {
				logger.info("AccountingDashboard: use configuration in scope: " + scope);
				if (accountingDashboardConfigJAXB.isEnabledInfraNode()) {
					logger.info("AccountingDashboard: Infra Nodes configuration enabled in scope: " + scope);
					InfraNodeJAXB infraNodeJAXB = accountingDashboardConfigJAXB.getBaseInfraNode();
					logger.info("AccountingDashboard: Base Infra Nodes configuration: " + infraNodeJAXB);
					if (infraNodeJAXB != null) {
						infraNode = new InfraNode(infraNodeJAXB.getScope(), infraNodeJAXB.getName(),
								infraNodeJAXB.getDescription());
						if (infraNodeJAXB.getChildren() != null && !infraNodeJAXB.getChildren().isEmpty()) {
							ArrayList<InfraNode> children = retrieveChildren(infraNodeJAXB);
							infraNode.setChildren(children);
						}

					} else {
						logger.info(
								"AccountingDashboard: Base Infra Nodes is not present in configuration"
								+ " resource for scope: "
										+ scope);
						//infraNode = useDefaultConfiguration();
					}
				} else {
					logger.info("AccountingDashboard: Infra Nodes configuration disabled for scope: " + scope);
				}
			} else {
				logger.info("AccountingDashboard: configuration resource is not present in scope: " + scope);
				// infraNode = useDefaultConfiguration();
			}
		}

		logger.debug("AccountingDashboard: Infra Nodes configuration set: " + infraNode);
		return infraNode;
	}

	private static InfraNode useDefaultConfiguration() {
		InfraNode infraNodeCoreServices = new InfraNode("CoreServices", "Core Services");
		InfraNode infraNodeCatalogue = new InfraNode("Catalogue", "Catalogue");
		InfraNode infraNodeWorkspace = new InfraNode("Workspace", "Workspace");
		InfraNode infraNodeMessages = new InfraNode("Messages", "Messages");

		ArrayList<InfraNode> children = new ArrayList<InfraNode>();
		children.add(infraNodeCatalogue);
		children.add(infraNodeWorkspace);
		children.add(infraNodeMessages);
		infraNodeCoreServices.setChildren(children);

		return infraNodeCoreServices;
	}

	private static ArrayList<InfraNode> retrieveChildren(InfraNodeJAXB infraNodeJAXB) throws ServiceException {
		try {
			ArrayList<InfraNode> children = new ArrayList<>();
			for (InfraNodeJAXB childJAXB : infraNodeJAXB.getChildren()) {
				InfraNode child = new InfraNode(childJAXB.getScope(), childJAXB.getName(), childJAXB.getDescription());
				if (childJAXB.getChildren() != null && !childJAXB.getChildren().isEmpty()) {
					ArrayList<InfraNode> childrenOfChild = retrieveChildren(childJAXB);
					child.setChildren(childrenOfChild);
				}
				children.add(child);
			}
			return children;

		} catch (Throwable e) {
			logger.error("Ivalid infra nodes configuration. Error retrieving children for infra nodes: "
					+ e.getLocalizedMessage(), e);
			throw new ServiceException("Ivalid infra nodes configuration. Error retrieving children for infra nodes: "
					+ e.getLocalizedMessage(), e);
		}

	}

}
