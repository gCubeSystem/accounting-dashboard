package org.gcube.portlets.user.accountingdashboard.shared.env;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public enum AccountingServiceType {

	Infrastructure, PortalContex, CurrentScope;

}
