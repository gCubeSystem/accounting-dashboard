package org.gcube.portlets.user.accountingdashboard.client.application.dialog.monitor;


import com.gwtplatform.mvp.client.UiHandlers;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public interface MonitorUiHandlers extends UiHandlers{
	
}
