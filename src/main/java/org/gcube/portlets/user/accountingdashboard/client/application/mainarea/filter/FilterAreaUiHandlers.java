package org.gcube.portlets.user.accountingdashboard.client.application.mainarea.filter;

import org.gcube.portlets.user.accountingdashboard.shared.data.RequestReportData;
import org.gcube.portlets.user.accountingdashboard.shared.options.TreeOptions;

import com.gwtplatform.mvp.client.UiHandlers;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public interface FilterAreaUiHandlers extends UiHandlers {

	public void getReport(RequestReportData requestReportData);

	public void updateTreeOptions(TreeOptions treeOptions);
}
