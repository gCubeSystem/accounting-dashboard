package org.gcube.portlets.user.accountingdashboard.shared.session;


/**
 * 
 * @author Giancarlo Panichi 
 *
 *
 */
public class SessionConstants {

	public static final String SAI_DESCRIPTOR="SAI_DESCRIPTOR";
	public static final String FILE_UPLOAD_MONITOR="FILE_UPLOAD_MONITOR";
	public static final String IMPORT_CODE_FILE_UPLOAD_SESSION = "IMPORT_CODE_FILE_UPLOAD_SESSION";
	public static final String PROJECT = "PROJECT";
	
	
}
