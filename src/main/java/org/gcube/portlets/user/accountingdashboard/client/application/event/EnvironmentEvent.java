package org.gcube.portlets.user.accountingdashboard.client.application.event;

import org.gcube.portlets.user.accountingdashboard.shared.env.EnvironmentData;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.event.shared.HasHandlers;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class EnvironmentEvent extends GwtEvent<EnvironmentEvent.EnvironmentEventHandler> {

	private EnvironmentData environmentData;

	public interface EnvironmentEventHandler extends EventHandler {
		void onInit(EnvironmentEvent event);
	}

	public static final Type<EnvironmentEventHandler> TYPE = new Type<>();

	public EnvironmentEvent(EnvironmentData environmentData) {
		this.environmentData = environmentData;
	}

	public static void fire(HasHandlers source, EnvironmentEvent event) {
		source.fireEvent(event);
	}

	@Override
	public Type<EnvironmentEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(EnvironmentEventHandler handler) {
		handler.onInit(this);
	}

	public EnvironmentData getEnvironmentData() {
		return environmentData;
	}

	@Override
	public String toString() {
		return "EnvironmentEvent [environmentData=" + environmentData + "]";
	}

}