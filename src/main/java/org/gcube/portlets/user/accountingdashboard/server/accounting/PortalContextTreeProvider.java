package org.gcube.portlets.user.accountingdashboard.server.accounting;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import org.gcube.accounting.accounting.summary.access.impl.ContextTreeProvider;
import org.gcube.accounting.accounting.summary.access.model.ScopeDescriptor;
import org.gcube.common.portal.GCubePortalConstants;
import org.gcube.common.portal.PortalContext;
import org.gcube.infrastructure.detachedres.detachedreslibrary.server.is.DetachedREsBuilder;
import org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re.DetachedREs;
import org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re.Gateway;
import org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re.VO;
import org.gcube.portlets.user.accountingdashboard.server.is.BuildInfraNode;
import org.gcube.portlets.user.accountingdashboard.server.util.CocktailSort;
import org.gcube.portlets.user.accountingdashboard.server.util.CocktailSort.Order;
import org.gcube.portlets.user.accountingdashboard.shared.env.AccountingServiceType;
import org.gcube.portlets.user.accountingdashboard.shared.is.InfraNode;
import org.gcube.portlets.user.accountingdashboard.shared.options.TreeOptions;
import org.gcube.vomanagement.usermanagement.GroupManager;
import org.gcube.vomanagement.usermanagement.impl.LiferayGroupManager;
import org.gcube.vomanagement.usermanagement.model.GCubeGroup;
import org.gcube.vomanagement.usermanagement.model.GCubeUser;
import org.gcube.vomanagement.usermanagement.model.VirtualGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Group;
import com.liferay.portal.model.VirtualHost;
import com.liferay.portal.service.LayoutSetLocalServiceUtil;
import com.liferay.portal.service.VirtualHostLocalServiceUtil;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class PortalContextTreeProvider implements ContextTreeProvider {

	private static GroupManager groupsManager;

	private static Logger logger = LoggerFactory.getLogger(PortalContextTreeProvider.class);
	private AccountingServiceType accountingServiceType;

	private TreeOptions treeOptions;

	public PortalContextTreeProvider(AccountingServiceType accountingServiceType) {
		this.accountingServiceType = accountingServiceType;
		this.treeOptions = null;
	}

	public void setTreeOptions(TreeOptions treeOptions) {
		this.treeOptions = treeOptions;
	}

	static {
		groupsManager = new LiferayGroupManager();
	}

	@Override
	public ScopeDescriptor getTree(Object context) throws Exception {
		if (context == null)
			throw new Exception("Unable to get tree, Request is null.");
		if (!(context instanceof HttpServletRequest))
			throw new Exception("Invalid request object : " + context);

		HttpServletRequest request = (HttpServletRequest) context;

		if (accountingServiceType == null) {
			logger.error("Invalid AccountingServiceType requested: null");
		}

		ScopeDescriptor root = null;
		switch (accountingServiceType) {
		case CurrentScope:
			logger.debug("AccountingService: CurrentScope");
			break;
		case PortalContex:
			logger.debug("AccountingService: PortalContext");
			root = recreateTreeForPortalContext(request);
			break;
		case Infrastructure:
			logger.debug("AccountingService: Infrastructure");
			root = recreateTreeForInfrastructure(request);
			break;
		default:
			logger.debug("AccountingService: CurrentScope");
			break;

		}

		return root;
	}

	private ScopeDescriptor recreateTreeForInfrastructure(HttpServletRequest request)
			throws Exception, PortalException, SystemException {
		ScopeDescriptor infra = null;
		PortalContext portalContext = PortalContext.getConfiguration();
		String infrastructureName = portalContext.getInfrastructureName();
		String infrastructureScope = portalContext.getCurrentScope(request);
		infra = new ScopeDescriptor(infrastructureName, infrastructureScope);

		LinkedList<ScopeDescriptor> infraChildren = new LinkedList<>();
		PortalContext pContext = PortalContext.getConfiguration();
		GCubeUser currUser = pContext.getCurrentUser(request);

		InfraNode infraNode = BuildInfraNode.build(infrastructureScope);

		List<GCubeGroup> theGateways = new LiferayGroupManager().getGateways();
		for (GCubeGroup gCubeGroup : theGateways) {
			logger.debug("Gateway: [id=" + gCubeGroup.getGroupId() + ", name=" + gCubeGroup.getGroupName() + "]");
		}
		for (GCubeGroup gCubeGroup : theGateways) {
			logger.debug("Gateway: [id=" + gCubeGroup.getGroupId() + ", name=" + gCubeGroup.getGroupName() + "]");

			LinkedList<ScopeDescriptor> gatewayChildren = retrieveGatewayChildren(request, gCubeGroup.getGroupId(),
					currUser);

			if (infraNode != null) {
				String scopeGroup = "";
				if (gCubeGroup.getGroupName().toUpperCase().endsWith(" GATEWAY")) {
					int l = gCubeGroup.getGroupName().length();
					scopeGroup = gCubeGroup.getGroupName().substring(0, l - 8);
				} else {
					scopeGroup = gCubeGroup.getGroupName();
				}

				StringBuilder gatewayScope = new StringBuilder();
				gatewayScope.append(infrastructureScope);
				gatewayScope.append("/");
				gatewayScope.append(scopeGroup);

				ScopeDescriptor infraNodeScopeDescriptor = createRelativeInfraNode(infraNode, gatewayScope.toString());
				if (gatewayChildren != null) {
					gatewayChildren.addFirst(infraNodeScopeDescriptor);
				} else {
					gatewayChildren = new LinkedList<>();
					gatewayChildren.add(infraNodeScopeDescriptor);
				}
			}

			ScopeDescriptor gatewayScopeDescriptor = new ScopeDescriptor(gCubeGroup.getGroupName(),
					gCubeGroup.getGroupId() + "");
			gatewayScopeDescriptor.setChildren(gatewayChildren);
			infraChildren.add(gatewayScopeDescriptor);
		}

		if (treeOptions != null && treeOptions.isShowDetachedREs()) {
			logger.debug("Show DestachedREs");
			DetachedREs detachedREs = DetachedREsBuilder.build(infrastructureScope);
			if (detachedREs != null && detachedREs.isEnabled()) {
				logger.debug("DetachedREs is enabled");
				if (detachedREs.getGateways() != null && !detachedREs.getGateways().isEmpty()) {
					ArrayList<Gateway> gatewaysList = new ArrayList<Gateway>(detachedREs.getGateways().values());
					Collections.sort(gatewaysList);
					for (Gateway gateway : gatewaysList) {
						ScopeDescriptor getewaySD = new ScopeDescriptor(gateway.getName(), gateway.getScope());
						LinkedList<ScopeDescriptor> voChildren = retrieveVOChildren(infraNode, gateway);
						getewaySD.setChildren(voChildren);
						infraChildren.add(getewaySD);
					}
					CocktailSort.sort(infraChildren,Order.DESC);
				}
			} else {
				logger.debug("DetachedREs is disabled");
			}
		}
		
		infra.setChildren(infraChildren);
		return infra;
	}

	private LinkedList<ScopeDescriptor> retrieveVOChildren(InfraNode infraNode, Gateway gateway) {
		LinkedList<ScopeDescriptor> vos = new LinkedList<>();

		if (infraNode != null) {
			ScopeDescriptor infraNodeScopeDescriptor = createRelativeInfraNode(infraNode, gateway.getScope());
			vos.add(infraNodeScopeDescriptor);
		}
		if (gateway.getVos() != null && !gateway.getVos().isEmpty()) {
			ArrayList<VO> vosList = new ArrayList<VO>(gateway.getVos().values());
			Collections.sort(vosList);
			for (VO vo : vosList) {
				ScopeDescriptor voSD = new ScopeDescriptor(vo.getName(), vo.getScope());
				LinkedList<ScopeDescriptor> voChildren = retrieveVREChildren(vo);
				voSD.setChildren(voChildren);
				vos.add(voSD);
			}
		}

		return vos;
	}

	private LinkedList<ScopeDescriptor> retrieveVREChildren(VO vo) {
		LinkedList<ScopeDescriptor> vres = new LinkedList<>();

		if (vo.getVres() != null && !vo.getVres().isEmpty()) {
			ArrayList<org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re.VRE> vresList = new ArrayList<org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re.VRE>(
					vo.getVres().values());
			Collections.sort(vresList);
			for (org.gcube.infrastructure.detachedres.detachedreslibrary.shared.re.VRE vre : vresList) {
				ScopeDescriptor vreSD = new ScopeDescriptor(vre.getName(), vre.getScope());
				vres.add(vreSD);
			}

		}
		return vres;
	}

	private ScopeDescriptor createRelativeInfraNode(InfraNode infraNode, String scope) {
		StringBuilder absoluteScope = new StringBuilder();
		absoluteScope.append(scope);
		absoluteScope.append("/");
		absoluteScope.append(infraNode.getScope());
		ScopeDescriptor scopeDescriptor = new ScopeDescriptor(infraNode.getName(), absoluteScope.toString());
		if (infraNode.getChildren() != null && !infraNode.getChildren().isEmpty()) {
			LinkedList<ScopeDescriptor> childsScopeDescriptor = new LinkedList<>();
			for (InfraNode child : infraNode.getChildren()) {
				ScopeDescriptor childScopeDescriptor = createRelativeInfraNode(child, scope);
				childsScopeDescriptor.add(childScopeDescriptor);
			}
			scopeDescriptor.setChildren(childsScopeDescriptor);
		}

		return scopeDescriptor;
	}

	private ScopeDescriptor recreateTreeForPortalContext(HttpServletRequest request)
			throws Exception, PortalException, SystemException {
		ScopeDescriptor root;

		Group group = getSiteFromServletRequest(request);
		long currentSiteGroupId = group.getGroupId();
		String gatewayName = group.getName();

		PortalContext pContext = PortalContext.getConfiguration();
		GCubeUser currUser = pContext.getCurrentUser(request);
		String portalScope = pContext.getCurrentScope(currentSiteGroupId + "");

		String scopeGroup = "";
		if (gatewayName.toUpperCase().endsWith(" GATEWAY")) {
			int l = gatewayName.length();
			scopeGroup = gatewayName.substring(0, l - 8);
		} else {
			scopeGroup = gatewayName;
		}
		StringBuilder gatewayScope = new StringBuilder();
		gatewayScope.append(portalScope);
		gatewayScope.append("/");
		gatewayScope.append(scopeGroup);

		InfraNode infraNode = BuildInfraNode.build(portalScope);

		LinkedList<ScopeDescriptor> rootChildren = null;
		rootChildren = retrieveGatewayChildren(request, currentSiteGroupId, currUser);

		if (infraNode != null) {
			ScopeDescriptor infraNodeScopeDescriptor = createRelativeInfraNode(infraNode, gatewayScope.toString());
			if (rootChildren != null) {
				rootChildren.addFirst(infraNodeScopeDescriptor);
			} else {
				rootChildren = new LinkedList<>();
				rootChildren.add(infraNodeScopeDescriptor);
			}
		}

		Group rootGroup = getSiteFromServletRequest(request);
		root = new ScopeDescriptor(rootGroup.getDescriptiveName(), rootGroup.getGroupId() + "");
		root.setChildren(rootChildren);
		logger.debug("TREE IS {} ", root);
		return root;
	}

	private LinkedList<ScopeDescriptor> retrieveGatewayChildren(HttpServletRequest request, long currentSiteGroupId,
			GCubeUser currUser) throws Exception, PortalException, SystemException {
		logger.info("Retrieve Gateway Children: currentSiteGroupId=" + currentSiteGroupId);

		// PARSE TREE
		LinkedHashMap<VRECategory, ArrayList<VRE>> gatewayTree = getPortalSitesMappedToVRE(currUser,
				currentSiteGroupId);

		logger.debug("Parsing tree from gateway. Size {} ", gatewayTree.size());

		LinkedList<ScopeDescriptor> rootChildren = new LinkedList<>();

		for (Entry<VRECategory, ArrayList<VRE>> entry : gatewayTree.entrySet()) {
			ScopeDescriptor rootChild = new ScopeDescriptor(entry.getKey().name, entry.getKey().categoryID + "");
			for (VRE vre : entry.getValue())
				rootChild.getChildren().add(new ScopeDescriptor(vre.name, vre.scope));
			rootChildren.add(rootChild);
		}
		return rootChildren;
	}

	/**
	 *
	 * @return the Virtual groups with their VREs in the order estabilished in
	 *         the LR Control Panel
	 * @throws SystemException
	 * @throws PortalException
	 */
	private LinkedHashMap<VRECategory, ArrayList<VRE>> getPortalSitesMappedToVRE(GCubeUser currUser,
			long currentSiteGroupId) throws Exception {

		LinkedHashMap<VRECategory, ArrayList<VRE>> toReturn = new LinkedHashMap<VRECategory, ArrayList<VRE>>();

		List<VirtualGroup> currentSiteVGroups = groupsManager.getVirtualGroups(currentSiteGroupId);

		for (VirtualGroup vg : currentSiteVGroups) {
			ArrayList<VRE> toCreate = new ArrayList<VRE>();
			VRECategory cat = new VRECategory(1L, vg.getName(), vg.getDescription());
			toReturn.put(cat, toCreate);
		}

		GCubeGroup rootGroupVO = groupsManager.getRootVO();

		try {
			logger.debug("root: " + rootGroupVO.getGroupName());
		} catch (NullPointerException e) {
			logger.error(
					"Cannot find root organziation, please check gcube-data.properties file in $CATALINA_HOME/conf folder, unless your installing the Bundle");
			return toReturn;
		}

		@SuppressWarnings("unused")
		List<GCubeGroup> currUserGroups = new ArrayList<GCubeGroup>();

		if (currUser != null) {
			currUserGroups = groupsManager.listGroupsByUser(currUser.getUserId());
		}

		// for each root sub organizations (VO)
		for (GCubeGroup vOrg : rootGroupVO.getChildren()) {
			for (GCubeGroup vreSite : vOrg.getChildren()) {
				long vreID = vreSite.getGroupId();
				String vreName = vreSite.getGroupName();
				String vreDescription = vreSite.getDescription();

				long logoId = vreSite.getLogoId();
				@SuppressWarnings("unused")
				String vreLogoURL = groupsManager.getGroupLogoURL(logoId);
				String infraScope = groupsManager.getInfrastructureScope(vreSite.getGroupId());
				String friendlyURL = GCubePortalConstants.PREFIX_GROUP_URL + vreSite.getFriendlyURL();

				List<VirtualGroup> vreGroups = groupsManager.getVirtualGroups(vreID);
				for (VirtualGroup vreGroup : vreGroups) {
					for (VRECategory vre : toReturn.keySet()) {
						if (vre.getName().compareTo(vreGroup.getName()) == 0) {
							ArrayList<VRE> toUpdate = toReturn.get(vre);
							VRE toAdd = new VRE(vreName, vreDescription, vreID, friendlyURL, infraScope);
							toUpdate.add(toAdd);
						}
					}
				}
			}
		}

		// sort the vres in the groups
		for (VRECategory cat : toReturn.keySet()) {
			ArrayList<VRE> toSort = toReturn.get(cat);
			Collections.sort(toSort);
		}
		return toReturn;
	}

	private Group getSiteFromServletRequest(final HttpServletRequest request) throws PortalException, SystemException {
		String serverName = request.getServerName();
		logger.debug("currentHost is " + serverName);
		Group site = null;
		List<VirtualHost> vHosts = VirtualHostLocalServiceUtil.getVirtualHosts(0,
				VirtualHostLocalServiceUtil.getVirtualHostsCount());
		for (VirtualHost virtualHost : vHosts) {
			logger.debug("Found " + virtualHost.getHostname());
			if (virtualHost.getHostname().compareTo("localhost") != 0 && virtualHost.getLayoutSetId() != 0
					&& virtualHost.getHostname().compareTo(serverName) == 0) {
				long layoutSetId = virtualHost.getLayoutSetId();
				site = LayoutSetLocalServiceUtil.getLayoutSet(layoutSetId).getGroup();
				logger.debug("Found match! Your site is " + site.getName());
				return site;
			}
		}
		return null;
	}

	private class VRECategory {
		private long categoryID;
		private String name;
		private String description;

		public VRECategory(long categoryID, String name, String description) {
			super();
			this.categoryID = categoryID;
			this.name = name;
			this.description = description;
		}

		@SuppressWarnings("unused")
		public long getCategoryID() {
			return categoryID;
		}

		@SuppressWarnings("unused")
		public String getDescription() {
			return description;
		}

		public String getName() {
			return name;
		}

		@Override
		public String toString() {
			return "VRECategory [categoryID=" + categoryID + ", name=" + name + ", description=" + description + "]";
		}

	}

	private class VRE implements Comparable<VRE> {
		private String name;
		private String description;
		private long id;
		private String url;
		private String scope;

		public VRE(String name, String description, long id, String url, String scope) {
			super();
			this.name = name;
			this.description = description;
			this.id = id;
			this.url = url;
			this.scope = scope;
		}

		@SuppressWarnings("unused")
		public String getDescription() {
			return description;
		}

		@SuppressWarnings("unused")
		public long getId() {
			return id;
		}

		public String getName() {
			return name;
		}

		@SuppressWarnings("unused")
		public String getScope() {
			return scope;
		}

		@SuppressWarnings("unused")
		public String getUrl() {
			return url;
		}

		@Override
		public String toString() {
			return "VRE [name=" + name + ", description=" + description + ", id=" + id + ", url=" + url + ", scope="
					+ scope + "]";
		}

		@Override
		public int compareTo(VRE vre) {
			return this.getName().compareTo(vre.getName());
		}
	}

}
