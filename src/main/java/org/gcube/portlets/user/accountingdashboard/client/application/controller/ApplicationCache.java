package org.gcube.portlets.user.accountingdashboard.client.application.controller;

import java.io.Serializable;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class ApplicationCache implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4076265357737593194L;
	
	public ApplicationCache() {

	}

	
	@Override
	public String toString() {
		return "ApplicationCache";
	}

}
