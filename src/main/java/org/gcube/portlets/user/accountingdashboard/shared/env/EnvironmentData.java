package org.gcube.portlets.user.accountingdashboard.shared.env;

import java.io.Serializable;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class EnvironmentData implements Serializable {

	private static final long serialVersionUID = 5817114786613046203L;

	private AccountingServiceType accountingServiceType;
	
	public EnvironmentData() {
		super();
	}

	public EnvironmentData(AccountingServiceType accountingServiceType) {
		super();
		this.accountingServiceType = accountingServiceType;
	}

	public AccountingServiceType getAccountingServiceType() {
		return accountingServiceType;
	}

	public void setAccountingServiceType(AccountingServiceType accountingServiceType) {
		this.accountingServiceType = accountingServiceType;
	}

	@Override
	public String toString() {
		return "EnvironmentData [accountingServiceType=" + accountingServiceType + "]";
	}

}
