package org.gcube.portlets.user.accountingdashboard.client.application.mainarea.report;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.logging.Logger;

import javax.inject.Inject;

import org.gcube.portlets.user.accountingdashboard.client.application.mainarea.report.chartjs.Chart;
import org.gcube.portlets.user.accountingdashboard.client.resources.AppResources;
import org.gcube.portlets.user.accountingdashboard.shared.data.ReportData;
import org.gcube.portlets.user.accountingdashboard.shared.data.ReportElementData;

import com.github.gwtbootstrap.client.ui.NavHeader;
import com.github.gwtbootstrap.client.ui.NavLink;
import com.github.gwtbootstrap.client.ui.Tab;
import com.github.gwtbootstrap.client.ui.TabPanel;
import com.github.gwtbootstrap.client.ui.WellNavList;
import com.github.gwtbootstrap.client.ui.base.IconAnchor;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;
import com.gwtplatform.mvp.client.ViewWithUiHandlers;

/**
 * 
 * @author Giancarlo Panichi
 *
 */
public class ReportAreaView extends ViewWithUiHandlers<ReportAreaPresenter>
		implements ReportAreaPresenter.ReportAreaView {

	private static Logger logger = java.util.logging.Logger.getLogger("");

	interface Binder extends UiBinder<Widget, ReportAreaView> {
	}

	@UiField
	HTMLPanel reportPanel;

	private HashMap<String, ArrayList<Chart>> categories;

	private AppResources resources;

	@Inject
	ReportAreaView(Binder uiBinder, AppResources resources) {
		this.resources = resources;
		init();
		initWidget(uiBinder.createAndBindUi(this));
	}

	private void init() {

	}

	@Override
	public void displayReportData(ReportData reportData) {
		if (reportData == null) {
			reportPanel.clear();
		} else {
			reportPanel.clear();
			
			/*HTMLPanel tabContent = new HTMLPanel("");
			for (int i = 0; i < reportData.getElements().size(); i++) {
				ReportElementData reportElementData = reportData.getElements().get(i);
				Chart chart = new Chart(resources, "report_" + i, reportElementData);
				tabContent.add(chart);
			}
			reportPanel.add(tabContent);
			*/
			
			categories = new HashMap<>();
			for (int i = 0; i < reportData.getElements().size(); i++) {
				ReportElementData reportElementData = reportData.getElements().get(i);
				String key = reportElementData.getCategory();
				ArrayList<Chart> category;
				if (categories.containsKey(key)) {
					category = categories.get(reportElementData.getCategory());
				} else {
					category = new ArrayList<>();
				}
				Chart chart = new Chart(resources, "report_" + i, reportElementData);
				category.add(chart);
				categories.put(key, category);
			}

			TabPanel tabPanel = new TabPanel();
			tabPanel.addStyleName(resources.uiDataCss().uiDataReportTabPanel());
			boolean first = true;
			ArrayList<String> sortedKeys = 
                    new ArrayList<String>(categories.keySet());         
			Collections.sort(sortedKeys);  
  
			for (String category : sortedKeys) {
				Tab tab = new Tab();
				tab.setHeading(category);
				HTMLPanel tabContent = new HTMLPanel("");
				if(categories.get(category).size()>0){
					WellNavList toc=new WellNavList();
					toc.addStyleName(resources.uiDataCss().uiDataReportToc());
					NavHeader navHeader=new NavHeader();
					if(categories.get(category).size()>1){
						navHeader.setText("Available Graphs");
					}else {
						navHeader.setText("Available Graph");
					}
					toc.add(navHeader);
					for (Chart chart : categories.get(category)) {
						ReportElementData reData=chart.getReportElementData();
						StringBuilder navLinkText=new StringBuilder();
						navLinkText.append(reData.getLabel());
						//navLinkText.append("[");
						//navLinkText.append(reData.getCategory());
						//navLinkText.append("]");
						NavLink navLink=new NavLink(navLinkText.toString(),"#"+chart.getWrapperName());
						navLink.setIcon(IconType.BAR_CHART);
						navLink.addStyleName(resources.uiDataCss().uiDataReportTocNavLink());
						toc.add(navLink);
					}
					tabContent.add(toc);
				}
				
				for (Chart chart : categories.get(category)) {
					tabContent.add(chart);
				}
				if (first) {
					tab.setActive(true);
					first = false;
				}
				tab.addClickHandler(new ClickHandler() {

					@Override
					public void onClick(ClickEvent event) {
						logger.fine("ClickEvent: " + event.getSource().getClass());
						IconAnchor iconAnchor = (IconAnchor) event.getSource();
						String category = iconAnchor.getText();
						if (category != null) {
							category = category.trim();
						}
						logger.fine("Category found: " + category);
						ArrayList<Chart> chartsInCategory = categories.get(category);
						// logger.fine("Charts List:"+chartsInCategory);
						if (chartsInCategory != null) {
							for (Chart chart : chartsInCategory) {
								chart.forceLayout();
							}
						}
					}
				});

				tab.add(tabContent);
				tabPanel.add(tab);
			}

			reportPanel.add(tabPanel);
			
		}

	}

}
