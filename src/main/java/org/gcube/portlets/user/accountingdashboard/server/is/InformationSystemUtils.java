package org.gcube.portlets.user.accountingdashboard.server.is;

import java.util.Iterator;
import java.util.List;

import org.gcube.common.resources.gcore.GenericResource;
import org.gcube.common.resources.gcore.ScopeGroup;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.portlets.user.accountingdashboard.shared.Constants;
import org.gcube.portlets.user.accountingdashboard.shared.exception.ServiceException;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.impl.JAXBParser;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.gcube.resources.discovery.icclient.ICFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */
public class InformationSystemUtils {

	private static Logger logger = LoggerFactory.getLogger(InformationSystemUtils.class);

	public static AccountingDashboardConfigJAXB retrieveAccountingDashboardConfig(String scope) throws ServiceException {
		try {

			if (scope == null || scope.length() == 0)
				return null;

			ScopeProvider.instance.set(scope);
			logger.debug("Retrieve Infra Node configuration in scope: " + scope);

			SimpleQuery query = ICFactory.queryFor(GenericResource.class);
			query.addCondition(
					"$resource/Profile/SecondaryType/text() eq '" + Constants.ACCOUNTING__DASHBOARD_CATEGORY + "'")
					.addCondition("$resource/Profile/Name/text() eq '" + Constants.ACCOUNTING_DASHBOARD_NAME + "'")
					.setResult("$resource");

			DiscoveryClient<GenericResource> client = ICFactory.clientFor(GenericResource.class);
			List<GenericResource> accountingResources = client.submit(query);
			logger.debug("Resources: " + accountingResources);

			AccountingDashboardConfigJAXB accountingDashboardConfigJAXB = null;

			for (GenericResource genericResource : accountingResources) {
				if (genericResource.scopes() != null) {
					ScopeGroup<String> scopes = genericResource.scopes();
					Iterator<String> iterator = scopes.iterator();
					String scopeFound = null;
					boolean found = false;
					while (iterator.hasNext() && !found) {
						scopeFound = iterator.next();
						if (scopeFound.compareTo(scope) == 0) {
							found = true;
						}
					}
					if (found) {
						try {
							JAXBParser<AccountingDashboardConfigJAXB> parser = new JAXBParser<AccountingDashboardConfigJAXB>(AccountingDashboardConfigJAXB.class);
							logger.debug("Body: " + genericResource.profile().bodyAsString());
							accountingDashboardConfigJAXB = (AccountingDashboardConfigJAXB) parser.parse(genericResource.profile().bodyAsString());
							logger.debug("Nodes: " + accountingDashboardConfigJAXB);
						} catch (Throwable e) {
							String error = "Error in discovery Accounting Dashboard config in scope "
									+ scope + ". " + "Resource parsing failed!";
							logger.error(error);
							logger.error(
									"Error {resource=" + genericResource + ", error=" + e.getLocalizedMessage() + "}");
							logger.error(e.getLocalizedMessage(), e);
							throw new ServiceException(error, e);
						}
						break;

					}

				}
			}

			return accountingDashboardConfigJAXB;

		} catch (ServiceException e) {
			throw e;
		} catch (Throwable e) {
			String error = "Error in discovery Accounting Dashboard config in scope: " + scope;
			logger.error(error,e);
			throw new ServiceException(error, e);
		}
	}

}
