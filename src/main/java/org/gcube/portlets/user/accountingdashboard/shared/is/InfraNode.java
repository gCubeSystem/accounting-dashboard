package org.gcube.portlets.user.accountingdashboard.shared.is;

import java.io.Serializable;
import java.util.ArrayList;

public class InfraNode implements Serializable {

	private static final long serialVersionUID = 2238683459962419017L;
	private String scope;
	private String name;
	private String description;
	private ArrayList<InfraNode> children;

	public InfraNode() {
		super();
	}

	public InfraNode(String scope, String name) {
		super();
		this.scope = scope;
		this.name = name;
	}

	public InfraNode(String scope, String name, String description) {
		super();
		this.scope = scope;
		this.name = name;
		this.description=description;
	}

	public InfraNode(String scope, String name, String description, ArrayList<InfraNode> children) {
		super();
		this.scope = scope;
		this.name = name;
		this.description = description;
		this.children = children;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ArrayList<InfraNode> getChildren() {
		return children;
	}

	public void setChildren(ArrayList<InfraNode> children) {
		this.children = children;
	}

	@Override
	public String toString() {
		return "InfraNode [scope=" + scope + ", name=" + name + ", description=" + description + ", children="
				+ children + "]";
	}

}