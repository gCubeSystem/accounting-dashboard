package org.gcube.portlets.user.accountingdashboard.server.is;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */

@XmlRootElement(name = "config")
@XmlAccessorType(XmlAccessType.FIELD)
public class AccountingDashboardConfigJAXB {

	@XmlElement
	private boolean enabledInfraNode;

	@XmlElement
	private InfraNodeJAXB baseInfraNode;

	public boolean isEnabledInfraNode() {
		return enabledInfraNode;
	}

	public void setEnabledInfraNode(boolean enabledInfraNode) {
		this.enabledInfraNode = enabledInfraNode;
	}

	public InfraNodeJAXB getBaseInfraNode() {
		return baseInfraNode;
	}

	public void setBaseInfraNode(InfraNodeJAXB baseInfraNode) {
		this.baseInfraNode = baseInfraNode;
	}

	@Override
	public String toString() {
		return "AccountingDashboardConfigJAXB [enabledInfraNode=" + enabledInfraNode + ", baseInfraNode="
				+ baseInfraNode + "]";
	}

	

}
