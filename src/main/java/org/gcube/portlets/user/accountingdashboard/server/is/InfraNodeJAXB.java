package org.gcube.portlets.user.accountingdashboard.server.is;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Giancarlo Panichi
 *
 *
 */

@XmlRootElement(name = "node")
@XmlAccessorType(XmlAccessType.FIELD)
public class InfraNodeJAXB {
	@XmlElement
	private String scope;

	@XmlElement
	private String name;

	@XmlElement(name = "description", required = false)
	private String description;

	@XmlElementWrapper(name = "children", required = false)
	@XmlElement(name = "node", required = false)
	private List<InfraNodeJAXB> children;

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<InfraNodeJAXB> getChildren() {
		return children;
	}

	public void setChildren(List<InfraNodeJAXB> children) {
		this.children = children;
	}

	@Override
	public String toString() {
		return "InfraNodeJAXB [scope=" + scope + ", name=" + name + ", description=" + description + ", children="
				+ children + "]";
	}

}
